var config = {
    entry: './main.js',
    output: {
        path:'/',
        filename: 'bundle.js',
    },
    devServer: {
        port: 7777
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            }
        ]
    }
}
module.exports = config;