import React from 'react'
import * as constants from './actions/actions'

import DigitButton from './components/DigitButton'
import Input from './components/Input'
import MathButton from './components/MathButton'

class App extends React.Component {
    render() {
        return (
            <div className = 'container'>      
                <div className = 'row no-gutters'>
                    <Input />   
                </div>
                <div className = 'row no-gutters'>
                    <MathButton action = {constants.c_Clear}/>
                    <MathButton action = {constants.c_Dot}/>
                    <MathButton action = {constants.c_Multiply}/>
                    <MathButton action = {constants.c_Divide}/>
                </div>
                <div className = 'row no-gutters'>
                    <DigitButton digit = {'7'}/>
                    <DigitButton digit = {'8'}/>
                    <DigitButton digit = {'9'}/>
                    <MathButton action = {constants.c_Minus}/>
                </div>  
                <div className = 'row no-gutters'>
                    <DigitButton digit = {'4'}/>
                    <DigitButton digit = {'5'}/>
                    <DigitButton digit = {'6'}/>
                    <MathButton action = {constants.c_Plus}/>
                </div>  
                <div className = 'row no-gutters'>
                    <DigitButton digit = {'1'}/>
                    <DigitButton digit = {'2'}/>
                    <DigitButton digit = {'3'}/>
                    <MathButton action = {constants.c_Equals}/>
                </div>  
            </div>
      );
   }
}

 export default App;