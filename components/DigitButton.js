import React, { Component } from 'react'

export default class DigitButton extends Component {
    render() {
        return (
            <div className = 'col-sm-1'>
                <button 
                    className = 'form-control border-danger btn-default' 
                    onClick = { (e) => this.handleClick(e)}>
                        {this.props.digit}
                </button>
            </div>
       )
    }
    handleClick(e) {
        document.getElementById('input').value += this.props.digit;
    }
}