import React, { Component } from 'react'
import * as constants from '../actions/actions'
import * as math from 'mathjs'

export default class MathButton extends Component {
    render() {
        return (
            <div className = 'col-sm-1'>
                <button 
                    className = 'form-control border-white btn-danger' 
                    onClick = { (e) => this.handleClick(e)}>
                        {this.props.action}
                </button>
            </div>
       )
    }
    handleClick(e) {
        let input = document.getElementById('input');

        switch (this.props.action)
        {
            case constants.c_Equals :
                input.value = math.eval(input.value);
                break;
            case constants.c_Clear :
                input.value = '';
                break;
            default :
                input.value += this.props.action;
                break;
        }
    }
}