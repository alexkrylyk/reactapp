import React, { Component } from 'react'

export default class Input extends Component {
    render() {
        return (
            <div className = 'col-sm-4'>
                <input type='text' className = 'form-control text-white bg-dark' id = 'input'></input>
            </div>
       )
    }
}